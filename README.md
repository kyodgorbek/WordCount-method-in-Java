# WordCount-method-in-Java


import java.io.*;
import java.util.*;

public class WordCount {
  // minimum number of occurences needed to printed
  
  public static void main(String[] args) throws FileNotFoundException{
   System.out.println("This program displays the most");
   System.out.println("Frequently occuring words from");
   System.out.println("the book Moby Dick.");
   System.out.println();
   
   //read the book into a map
   Scanner in = new Scanner(new File("mobydick.txt"));
   Map<String, Integer> wordCountMap = getCountMap(in);
   
   for (String word:  wordCountMap.keySet()) {
     int count = wordCountMap.get(word);
     if(count > OCCURRENCES) {
      System.out.println(word + "occurencs" + count + " times.");
     }
    }
   }   

  Reads book text and returns a map from words to counts
  public static Map<String, Integer> getcountMap(Scanner in){
   Map<String, Integer> wordCountmap = new TreeMap<String, Integer>();
   
   while (in.hasNext()){
    String word = int.next().toLowerCase();
    if(wordCountMap.containsKey(word) {
     //never seen this word before
     wordCountMap.put(word, 1);
   } else {
    // seen  this word before; increment count
    int count = wordCountMap.get(word);
    wordCountMap.put(word, count + 1);
   }
  }
  
  return wordCountMap;
 }
}    
    
    
    
